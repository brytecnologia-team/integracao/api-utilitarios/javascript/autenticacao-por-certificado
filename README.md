# Verificação de Desafio para Autenticação

Este é um exemplo de integração dos serviços da API de validação de certificados para realizar a prova de posse de um certificado digital com clientes baseados em tecnologia Node.js. 

Este exemplo apresenta os passos necessários para realizar a prova de posse de um certificado digital:
  - Passo 1 (Servidor): Geração de um desafio que será utilizado durante a prova de posse.
  - Passo 2 (Cliente): Carregamento da chave privada e conteúdo do certificado digital.
  - Passo 3 (Cliente): Assinatura do desafio utilizando a chave privada do certificado.
  - Passo 4 (Servidor): Verificação da assinatura do desafio e do certificado digital utilizado para realizar a assinatura.

**Observação**

A validação da assinatura do desafio e do certificado é realizada de maneira independente, portanto para provar a posse da chave privada correspondente àquele certificado é necessário verificar tanto o status da assinatura quanto o status do certificado.

### Tech

O exemplo utiliza as bibliotecas JavaScript abaixo:
* [jsrsasign] - Biblioteca que fornece recursos de funções criptográficas;
* [Forge] - O software Forge é uma implementação totalmente nativa do protocolo TLS, um conjunto de utilitários de criptografia e um conjunto de ferramentas para o desenvolvimento de aplicativos Web que utilizam muitos recursos de rede;
* [Request] - Bibliotecas para fazer chamadas http.

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

Além disso, no processo de geração de assinatura é obrigatório a posse de um certificado digital que identifica o autor do artefato assinado que será produzido.

Por esse motivo, é necessário configurar a localização deste arquivo no computador, bem como a senha para acessar seu conteúdo.

**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para cifragem do desafio e verificação de posse.

| Variável | Descrição | Classe de Configuração |
| ------ | ------ | ------ |
| TOKEN | Access Token para o consumo do serviço (JWT). | ServiceConfig
| CAMINHO_CERTIFICADO | Localização do arquivo da chave privada a ser configurada na aplicação. | CertificateConfig
| SENHA_CERTIFICADO | Senha do arquivo da chave privada a ser configurada na aplicação. | CertificateConfig
| MODE | Modo de retorno da validação do certificado. | ServiceConfig
| HASH_ALGORITHM | Algoritmo de hash utilizado na assinatura do desafio. | ServiceConfig

## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).

### Uso

   [jsrsasign]: <http://kjur.github.io/jsrsasign/>
   [Forge]: <https://github.com/digitalbazaar/forge>
   [Request]: <https://github.com/request/request>


<b>1</b> - As dependências podem ser instaladas utilizando o gerenciador de pacotes npm ou yarn.

Na pasta raiz do projeto, execute o comando de instalação conforme o gerenciador de dependências de sua preferência:

Instalando as dependências com npm:

    npm install

Instalando as dependências com yarn:

    yarn

<b>2</b> - Para executar a aplicação, execute o comando:

    node src/main/app.js