const request = require('request');
const { Base64Binary } = require('../functions/Base64Decoder');
const serviceConfig = require('../config/ServiceConfig');
const { cipher, getPublicKey, setPublicAndPrivateKey } = require('../functions/Cipher');

//Gera uma String aleatória em base64 para ser assinada (Desafio)
function gerarDesafio() {
    var characters       = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    var charactersLength = characters.length;
    var desafio = '';
    for ( var i = 0; i < 64; i++ ) {
        desafio += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    request
    return Buffer.from(desafio).toString('base64');
}

//Verifica desafio
function verificarDesafio(certificado, desafio, cifrado) {
    //Json com os dados da requisição
    var jsonData = 
        {
            "certificate": certificado,
            "mode" : serviceConfig.MODE,
            "hashAlgorithm" : serviceConfig.HASH_ALGORITHM,
            "challenge": desafio,
            "challengeSignature": cifrado
        };

    //Headers da requisição
    headers = {
        'Content-Type': 'application/json',
        'Authorization': serviceConfig.TOKEN
    };

    console.log("============= Inicializando Verificação de Desafio no BRy HUB =============\n")
    console.log("Desafio: " +  desafio);

    return new Promise((resolve) => {
        request.post({ url: serviceConfig.URL_DESAFIO, json: jsonData, headers: headers }, (error, response, body) => {
            if (response.statusCode == 200) {
                console.log("\nDesafio verificado com sucesso.\n");
                var statusCertificado = body.certificateReport.status.status;
                var statusAssinatura = body.signatureVerified;

                console.log("O certificado do assinante está com status: " + statusCertificado);
                console.log("A assinatura do desafio esta com valor: " + statusAssinatura + "\n");

                if (statusCertificado == 'VALID' && statusAssinatura == 'true') {
                    resolve(true);
                }
            } else {
                console.log(body);
                console.log("");
            }
            resolve(false);
        })
    })
}

//Passo 1:
//Gera o desafio que será assinado pelo certificado. Este passo deve ser realizado pelo servidor de autenticação.
var desafio = gerarDesafio();

//Passo 2:
//Carrega a chave privada e o conteúdo do certificado digital.
setPublicAndPrivateKey();
certificadoDigital = getPublicKey();

//Passo 3:
//Assina o desafio utilizando o certificado em disco. Resultado da assinatura também deve estar codificado em base64.
cifrado = cipher(Buffer.from(Base64Binary.decode(desafio), 'binary').toString('hex'));

//Passo 4:
//Realiza a requisição para o HUB para validação da assinatura e certificado, verificando se o desafio foi aceito. Este passo deve ser realizado pelo servidor de autenticação.
verificarDesafio(certificadoTratado, desafio, cifrado).then(result => {
    if (result) {
        console.log('Usuário autenticado com sucesso!');
    } else {
        console.log('Usuário não pode ser autenticado.');
    }
});
