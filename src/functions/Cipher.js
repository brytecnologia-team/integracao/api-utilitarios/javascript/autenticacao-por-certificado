var forge = require('node-forge');
var KJUR = require('jsrsasign')
var fs = require('fs');
const certificateConfig = require('../config/CertificateConfig');
const serviceConfig = require('../config/ServiceConfig');

var privateKey;
var publicKey;

function setPublicAndPrivateKey() {
    console.log('Define chave public e privada...\n');

    var p12Asn1 = forge.asn1.fromDer(
        fs.readFileSync(certificateConfig.CAMINHO_CERTIFICADO, 'binary'));

    var pkcs12 = forge.pkcs12.pkcs12FromAsn1(p12Asn1, false, certificateConfig.SENHA_CERTIFICADO);

    //Then get the private key from pkcs12 of the desired certificate (see forge doc) and convert to PKCS # 8 to be imported with webcrypto
    // load keypair and cert chain from safe content(s) 
    for (var sci = 0; sci < pkcs12.safeContents.length; ++sci) {
        var safeContents = pkcs12.safeContents[sci];

        for (var sbi = 0; sbi < safeContents.safeBags.length; ++sbi) {
            var safeBag = safeContents.safeBags[sbi];

            if (safeBag.cert != undefined) {
                // DER-encode certificate and then base64-encode that
                var der = forge.asn1.toDer(forge.pki.certificateToAsn1(safeBag.cert));
                var b64 = forge.util.encode64(der.getBytes());

                this.publicKey = b64;
            }

            // this bag has a private key
            if (safeBag.type === forge.pki.oids.keyBag) {
                //Found plain private key
                privateKey = safeBag.key;
            } else if (safeBag.type === forge.pki.oids.pkcs8ShroudedKeyBag) {
                // found encrypted private key
                privateKey = safeBag.key;
            } else if (safeBag.type === forge.pki.oids.certBag) {
                // this bag has a certificate...        
            }
        }
    }
}

function getPublicKey() {
    return this.publicKey;
}

// Converte hex string para byte array
function hexToBytes(hex) {
    for (var bytes = [], c = 0; c < hex.length; c += 2)
        bytes.push(parseInt(hex.substr(c, 2), 16));
    return bytes;
}

const cipher = (conteudo) => {
    console.log("================  Inicializando assinatura  ================\n")

    var pki = forge.pki;

    // wrap an RSAPrivateKey ASN.1 object in a PKCS#8 ASN.1 PrivateKeyInfo
    var privateKeyInfo = pki.wrapRsaPrivateKey(pki.privateKeyToAsn1(privateKey));

    // convert a PKCS#8 ASN.1 PrivateKeyInfo to PEM
    var pem = pki.privateKeyInfoToPem(privateKeyInfo)

    var hashAlgorithm = serviceConfig.HASH_ALGORITHM + 'withRSA';

    var sig = new KJUR.crypto.Signature({ "alg": hashAlgorithm });

    sig.init(pem);
    sig.updateHex(conteudo);
    var signedData = sig.sign();

    var signedDataInBinary = hexToBytes(signedData);

    var signedDataBase64 = Buffer.from(signedDataInBinary, 'binary').toString('base64');
    console.log("Valor da assinatura em Base64: " + signedDataBase64);
    console.log("");

    return signedDataBase64;
}

module.exports = { cipher, getPublicKey, setPublicAndPrivateKey };